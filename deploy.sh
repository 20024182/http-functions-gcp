#!/usr/bin/env bash
#
# Deploy HTTP Cloud Functions in Java on API Gateway using the gcloud command-line
# tool

set -e

# Constants
readonly API_ID='prime'
readonly API_DEFINITION='swagger.yaml'
readonly CONFIG_ID='prime-config'
readonly GATEWAY_ID='my-first-gateway'

function enable_apis() {
  gcloud services enable apigateway.googleapis.com
  gcloud services enable servicemanagement.googleapis.com
  gcloud services enable servicecontrol.googleapis.com
  gcloud services enable cloudfunctions.googleapis.com
  gcloud services enable cloudbuild.googleapis.com
}

function deploy_cloud_function() {
  gcloud functions deploy prime --quiet --region=europe-west1 \
    --entry-point 'functions.Prime' --trigger-http --runtime java11
}

function setup_api_gateway() {
  gcloud api-gateway apis create "${API_ID}"
  sed -ie "s@\${ADDR}@$(gcloud functions describe prime --region=europe-west1 --format='value(httpsTrigger.url)')@" "${API_DEFINITION}"
  gcloud api-gateway api-configs create "${CONFIG_ID}" \
    --api="${API_ID}" --openapi-spec="${API_DEFINITION}" \
    --backend-auth-service-account="$(gcloud iam service-accounts list --filter="displayName:App Engine default service account" --format="value(email)")"
  gcloud api-gateway gateways create "${GATEWAY_ID}" \
    --api="${API_ID}" --api-config="${CONFIG_ID}" \
    --location=us-central1
}

function print_endpoint() {
  echo "Your endpoint is https://$(gcloud api-gateway gateways describe "${GATEWAY_ID}" \
    --location=us-central1 --format="value(defaultHostname)")/prime"
}

function main() {
    enable_apis
    deploy_cloud_function
    setup_api_gateway
    print_endpoint
}

main
