# http-functions-gcp

Project used in the HPC exam tutorial (**Deploy HTTP Cloud Functions in Java on API Gateway using the `gcloud`
command-line tool**).

## How to deploy locally?

Open terminal and run:

```console
$ git clone https://gitlab.di.unipmn.it/20024182/http-functions-gcp.git
$ cd http-functions-gcp
$ chmod +x ./mvnw
$ JAVA_HOME=/usr/lib/jvm/graalvm-ee-java16-21.1.0/ ./mvnw function:run # set JAVA_HOME to use a specific version of Java. 
                                                                       # Minimum version JDK 11
```

Run in a new terminal:

```console
$ curl --location --request POST 'http://localhost:8080/' \
  -s -w " | HTTP Status-Code: %{response_code}\n" \
  --header 'Content-Type: application/json' \
  --data-raw '{
      "number": "5832158674351"
  }'
```

to test this function. Output achieved

```text
<response> | <HTTP Status-Code>
```

where

* `<response>` is a JSON string that contains `true` if the number in `--data-raw` option is a prime, `false` otherwise.
  If an error occurred this field is empty.
* `<HTTP Status-Code>` a numerical constant defined in RFC2616.
