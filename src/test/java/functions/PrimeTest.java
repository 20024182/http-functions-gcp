package functions;

import com.google.cloud.functions.HttpRequest;
import com.google.cloud.functions.HttpResponse;
import com.google.common.testing.TestLogHandler;
import com.google.gson.Gson;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.*;
import java.net.HttpURLConnection;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Logger;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.Mockito.*;

/**
 * Test for {@code Prime} class.
 *
 * @author Lorenzo Ferron
 * @version 20210817
 */
@RunWith(JUnit4.class)
public class PrimeTest {

    private static final Gson GSON = new Gson();
    private static final Logger LOGGER = Logger.getLogger(Prime.class.getName());
    private static final TestLogHandler LOG_HANDLER = new TestLogHandler();
    @Mock
    private HttpRequest request;
    @Mock
    private HttpResponse response;
    private BufferedWriter writerOut;
    private StringWriter responseOut;

    @BeforeClass
    public static void setUp() {
        LOGGER.addHandler(LOG_HANDLER);
    }

    @Before
    public void beforeTest() throws java.io.IOException {
        MockitoAnnotations.openMocks(this);

        this.responseOut = new StringWriter();
        this.writerOut = new BufferedWriter(this.responseOut);
        when(this.response.getWriter()).thenReturn(this.writerOut);

        LOG_HANDLER.clear();
    }

    @Test
    public void parseContentType() throws IOException {
        // Send a request with JSON data
        String requestJson = GSON.toJson(Map.of("number", "31"));
        BufferedReader bodyReader = new BufferedReader(new StringReader(requestJson));

        when(this.request.getMethod()).thenReturn("POST");
        when(this.request.getContentType()).thenReturn(Optional.of("application/json"));
        when(this.request.getReader()).thenReturn(bodyReader);

        new Prime().service(this.request, this.response);

        assertThat(LOG_HANDLER.getStoredLogRecords().get(0).getMessage()).isEqualTo("Processed number: 31");

        this.writerOut.flush();
        assertThat(this.responseOut.toString()).isEqualTo("{\"response\":true}");
    }

    @Test
    public void negativeNumberInBody() throws IOException {
        // Send a request with JSON data
        String requestJson = GSON.toJson(Map.of("number", "-2"));
        BufferedReader bodyReader = new BufferedReader(new StringReader(requestJson));

        when(this.request.getMethod()).thenReturn("POST");
        when(this.request.getContentType()).thenReturn(Optional.of("application/json"));
        when(this.request.getReader()).thenReturn(bodyReader);

        new Prime().service(this.request, this.response);

        verify(this.response, times(1)).setStatusCode(HttpURLConnection.HTTP_BAD_REQUEST);
    }

    @Test(expected = NumberFormatException.class)
    public void emptyBody() throws IOException {
        // Send a request with JSON data
        String requestJson = GSON.toJson(Map.of("number", ""));
        BufferedReader bodyReader = new BufferedReader(new StringReader(requestJson));

        when(this.request.getMethod()).thenReturn("POST");
        when(this.request.getContentType()).thenReturn(Optional.of("application/json"));
        when(this.request.getReader()).thenReturn(bodyReader);

        new Prime().service(this.request, this.response);
    }

    @Test
    public void parseUnknownContentTypeTest() throws IOException {
        // Send a request with JSON data
        String requestJson = GSON.toJson(Map.of("number", "31"));
        BufferedReader bodyReader = new BufferedReader(new StringReader(requestJson));

        when(this.request.getMethod()).thenReturn("POST");
        when(this.request.getContentType()).thenReturn(Optional.of("application/unknown"));
        when(this.request.getReader()).thenReturn(bodyReader);

        new Prime().service(this.request, this.response);

        verify(this.response, times(1)).setStatusCode(HttpURLConnection.HTTP_UNSUPPORTED_TYPE);
    }

    @Test
    public void functionShouldErrorOnGet() throws IOException {
        when(this.request.getMethod()).thenReturn("GET");

        new Prime().service(this.request, this.response);

        this.writerOut.flush();
        verify(this.response, times(1)).setStatusCode(HttpURLConnection.HTTP_BAD_METHOD);
    }
}